# Third-party partners

The following is a list of third-party organisations that we may share the data you share with us with. This sharing is conducted in line with the objectives set out in our privacy policy, however a summary of the purposes are also listed here.

| Organisation | Data shared | Purpose | URL |
|----------|---------|--------|-------|
| DroneBL | IP address & hostname | Prevention of bot/spam abuse | https://www.dronebl.org/|
| Sectoor | IP address & hostname | Blocks Tor exit nodes | N/A |
| The Scout Association | Scout Association membership number, full name | Membership status validation | https://scouts.org.uk |
| World Organisation of the Scouting Movement | National Scouting Organisation membership number, full name, email address | Registration for JOTI, membership validation | https://scout.org |
| 